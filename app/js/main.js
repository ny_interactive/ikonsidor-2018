window.globals = {
  $root: $('html,body'),
  scrollTop: $(window).scrollTop(),
  windowHeight: $(window).height(),
  windowWidth: $(window).width(),

  orientation: function(){
    globals.windowWidth = $(window).width();
    globals.windowHeight = $(window).height();
    console.log('orientation - h: ', globals.windowHeight , ', w: ', globals.windowWidth, ' - ', (globals.windowHeight / globals.windowWidth) );
    if( (globals.windowHeight / globals.windowWidth) > 1 ){
    // if( (globals.windowHeight / globals.windowWidth) > 0.7 ){
      // return 'portrait';
      $('html').removeClass('landscape');
      $('html').addClass('portrait');
    }
    else {
      // return 'landscape';
      $('html').addClass('landscape');
      $('html').removeClass('portrait');
    }
  },

  updateScrollPos : function(){
    globals.scrollTop = $(window).scrollTop();
  }
}

window.mouse = {
  posX:0,
  posY:0,

  init: function(){
    $(window).on('mousemove', function(e){
      mouse.posX = e.pageX;
      mouse.posY = e.pageY;
    });
  }
}


window.navigation = {
  wrapper: $('.top-header'),
  lastScrollPos: 0,

  init: function(){
    navigation.toggleMenu();
    navigation.activateHiddenModals();

    $('.canvas-toggle, .off-canvas').on('click', function(){
      $('body').toggleClass('canvas-open');
    });

    $('[go-to-id]').on('click', function(e){
      e.preventDefault();
      var _id = '#' + $(this).attr('go-to-id');
      navigation.scrollToId(_id);
    });

    $('.play-movie').on('click', function(e){
      e.preventDefault();
      //
      // console.log('offset: ' + $(this).offset().top);
      // console.log('position: ' + $(this).position().top);
      var $this = $(e.target);

      var $movie;
      // console.log('$this.parent(): ', $this.parent().hasClass('movie-wrapper'));

      if( $this.attr('movie-id') != undefined ){
        var movieId = $this.attr('movie-id');

        $movie = $('#' + movieId ),
        $teaser = ( $('#teaser_' +movieId ).length > 0 ) ? $('#teaser_' +movieId ) : false;

        if( $teaser ){

          $teaser.addClass('paused');

          $tvideo = $teaser.get(0);
          $tvideo.pause();
          console.log('teaser: ', $teaser);


        }

        if( $movie.hasClass('youtube') ){
          var _src = $movie.attr('src');

    			if( _src.indexOf('autoplay') > -1 ){
    				var _newSrc = _src.replace('autoplay=0', 'autoplay=1');

    				_src = _newSrc;
    			}
    			else {
    				_src += '&autoplay=1';
    			}

    			$movie.attr('src', _src);
        }
        else {
          $movie.get(0).play();

        }


        navigation.checkVideoTime($movie.get(0));

      }

      // var $wrapper = $this.parent(),
      var $wrapper = $movie.parent(),
          $video = $wrapper.find('video').get(0);


      if( $movie.parent().hasClass('movie-wrapper') || $movie.parent().hasClass('product-spec__video') ){
        //$video.play();

        $movie.parent().addClass('playing');


      }

      else if ( $movie.parent().parent().hasClass('movie-wrapper') ){
        // $this.parent().parent().find('video').get(0).play();
        //
        // setTimeout(function () {
        //
        //   $this.parent().parent().addClass('playing');
        // }, 100);

        setTimeout(function () {

          $movie.parent().parent().addClass('playing');
        }, 100);

      }
      else {



        var $videoWrapper = $this.parent().find('.popup-video__overlay'),
            // $videoWrapper = $wrapper.find('.video-wrapper'),
            _posX = $this.offset().left,
            _posY = $this.offset().top;

          //  $video.play();

        $videoWrapper.css({
          'left': mouse.posX - $this.width()/2,
          'top': mouse.posY - globals.scrollTop
        });


        setTimeout(function () {
          // $('.product-spec__video').addClass('playing');
          $this.parent().addClass('playing');

          $('body').addClass('freeze');

        }, 100);

        setTimeout(function () {
          // $('.product-spec__video').addClass('center');
          $this.parent().addClass('center');
        }, 800);

      }




    });

    $('.video-wrapper').on('click', function(){

      navigation.closeVideo();

    });


    $('a[href*="/"]').on('click', function(e){

      if( !$(this).parent().hasClass('menu-item-has-children') && $(this).attr('target') == undefined ){

        var _href = $(this).attr('href');
        e.preventDefault();

        $('body').addClass('page-switch');

        setTimeout(function () {
          window.location.href = _href;
        }, 200);
      }

    });


    $(document).keyup(function(e) {

      switch (e.keyCode){

        // Esc
        case 27:
          navigation.closeVideo();
          break;

        default:
          break;
      }
    });

  },

  checkVideoTime : function(_video){

		var _timer = setInterval(function () {


			var _diff = _video.duration - _video.currentTime;
			console.log('diff: ', _diff);


			if( _diff < 1 ){
				var _movieWrapper = $('.movie-wrapper.playing');
				clearInterval(_timer);
				//movieControls.endMovie( _movieWrapper );
        navigation.endMovie(_movieWrapper);

				setTimeout(function () {
					_movieWrapper.removeClass('ending');
				}, 500);

			}
			else if( _diff < 10 ){
				$('.movie-wrapper.playing').addClass('ending');
			}

		}, 100);

	},

  endMovie: function( _movieWrapper ){
    _movieWrapper.removeClass('playing');

  },

  closeVideo: function(){
    var $video = $('.product-spec__video.playing').find('video').get(0);
    var $videoWrapper = $('.product-spec__video.playing').find('.popup-video__overlay');

    $('.product-spec__video').removeClass('playing center');


    $('body').removeClass('freeze');

    if($video){


      setTimeout(function () {
        $video.pause();

        $videoWrapper.css({
          'left': 'auto',
          'top': 'auto',
          // 'margin': 'auto'
        });

      }, 500);
    }
    else {

      var $movie = $('iframe[src*="autoplay=1"]');

      console.log('movie: ' , $movie);

      var _src = $movie.attr('src');

        var _newSrc = _src.replace('autoplay=1', 'autoplay=0');

        _src = _newSrc;


      $movie.attr('src', _src);

      setTimeout(function () {

        $videoWrapper.css({
          'left': 'auto',
          'top': 'auto',
          // 'margin': 'auto'
        });

      }, 500);

    }
  },

  activateHiddenModals: function(){

    $('.hidden-modal').on('click', function(){
      $(this).toggleClass('open');
    })

  },

  toggleScroll: function(){

    var dir = ( navigation.lastScrollPos > globals.scrollTop ) ? 'up' : 'down';
    var $body = $('body');



    if( dir === 'up'){
      // navigation.wrapper.removeClass('scrolling');
      $body.addClass('up');
    }
    else if( globals.scrollTop > 152 && $('.menu-item-has-children:hover').length < 1 && $('.menu-item-has-children.sub-open').length < 1){
      $body.addClass('scrolling');
      $body.removeClass('up');

    }
    else {
      $body.removeClass('scrolling');
      $body.removeClass('up');

    }

    navigation.lastScrollPos = globals.scrollTop;

  },

  scrollToId: function(_id){
    globals.$root.animate({
      	scrollTop: $(_id).offset().top -60
  	}, 500);
  },

  toggleMenu: function(){

    $('.menu-toggle').on('click', function(e){
      e.preventDefault();

      $('body').toggleClass('open-menu');

      $('.open-menu .site').on('click', function(){
        $('body').removeClass('open-menu');
      });
    });

    $('.menu-item-has-children > a').on('click', function(e){
      e.preventDefault();

      $(this).parent().toggleClass('sub-open');
    });

    $('.site').on('click', function(){
      if( $('.menu-item-has-children.sub-open').length > 0 ){
        $('.menu-item-has-children.sub-open').removeClass('sub-open');
      }
    });



  }
}


window.mirror = {
  bg: $('.mirror__bg'),
  textClip: $('.mirror .mirror__clip'),

  render: function(){

    mirror.textClip.css({
      'background': 'url(' + mirror.bg.attr('src') + ')',
      'background-size': '100vw auto',
      'background-clip': 'text',
      'background-attachment': 'fixed',
      '-webkit-background-clip': 'text',
      '-webkit-text-fill-color': 'transparent',
      'text-fill-color': 'transparent',
      'background-position': 'center top',
      'transform':'translate3d(0,' + $(window).scrollTop()/4 + 'px,0)',
       // 'background-position':'left -'+ ( ( $(window).scrollTop()/5 )+ globals.windowHeight/5) + 'px'
    });

    mirror.bg.css({
      // 'background-position':'center -'+ $(window).scrollTop()/4 + 'px'
      'transform':'translate3d(0,'+ $(window).scrollTop()/8 + 'px,0)'
    });

  }
}



window.scrollPoints = {
  scrollTop: 0,
  windowHeight: $(window).height(),
  scrollElements: $('.scroll-point__scroll'),
  zoomElements: $('.scroll-point__zoom'),
  showElements: $('.scroll-point__show'),
  triggers: $('.scroll-point'),
  scrollDir:'down',
  lastScrollPos:0,

  render: function(){
    scrollPoints.calculateVariables();
    scrollPoints.checkElements();
    scrollPoints.animateOnScroll();
  },

  calculateVariables: function(){
    scrollPoints.scrollTop = $(window).scrollTop();
    scrollPoints.windowHeight = $(window).height();
    scrollPoints.bottom_of_window = scrollPoints.scrollTop + scrollPoints.windowHeight;
  },

  checkElements: function() {

    scrollPoints.triggers.each(function() {
      var $this = $(this);
      var offset = $this.attr('offset') ? $this.attr('offset') : 0;

      // var scrollOffset = ( offset.toString().indexOf('vh') >= 0 ) :


      if ( offset.toString().indexOf('%') >= 0 ){
        offset = offset.split('%')[0];
        offset = $this.height() * (offset/100);
      }
      else if ( offset.toString().indexOf('vh') >= 0 ){
        offset = offset.split('vh')[0];
        offset = globals.windowHeight * (offset/100);
      }

      var top_of_object = $this.offset().top + offset;

      if( scrollPoints.bottom_of_window > top_of_object - offset ) {
        $this.addClass('scroll-point--ready');
        // scrollPoints.animateOnScroll($this);
      }
      else {
        $this.removeClass('scroll-point--ready');

      }

      if( scrollPoints.bottom_of_window > top_of_object ) {

        // if( $this.parent().hasClass('scroll-point--active') && $this.hasClass('scroll-point--active') ){
        //   $('body').addClass('freeze');
        //
        //   setTimeout(function () {
        //     $('body').removeClass('freeze');
        //
        //   }, 300);
        // }

        if( !$this.hasClass('scroll-point--active') ){
          $this.addClass('scroll-point--active');

          // console.log('ID: ' + $this.attr('id') + ', ' + $this.attr('trigger-hotspots'));

          var triggered = false;

          if ( $this.attr('trigger-hotspots') != undefined && !triggered && !$this.hasClass('scroll-point--passive')){
            triggered = true;
            //console.log('nu ska den trigga');
            //console.log('ID: ' + $this.attr('id') + ', ' + $this.attr('trigger-hotspots'));


            setTimeout(function () {
              hotspots.flashHotspots($this.attr('trigger-hotspots'));

            }, 200);
          }

        }

        scrollPoints.animateOnScroll($this);


      }
      else {
        $this.removeClass('scroll-point--active');
        $this.removeClass('passive');
      }




      if( scrollPoints.bottom_of_window >= ( $this.height() + $this.offset().top )){

        // if ( !$this.hasClass('scroll-point__section')){
          $this.addClass('scroll-point--passive');
          // $this.removeClass('scroll-point--ready');

  				// $this.removeClass('scroll-point--active');
        // }


      }
      else {
        $this.removeClass('scroll-point--passive');

      }




    });


    scrollPoints.scrollDir = ( scrollPoints.lastScrollPos > globals.scrollTop ) ? 'up' : 'down';




    // if( scrollPoints.scrollDir === 'up' || globals.scrollTop < 40 ){
    //   $($('.scroll-point--active').get( $('.scroll-point--active').length - 2 )).removeClass('passive');
    //
    // }

    scrollPoints.lastScrollPos = scrollPoints.scrollTop;

  },

  animateOnScroll: function( $triggerSection ){
    var animate_on_scroll = true;

    if( $('html').hasClass('css-animate') ){
      animate_on_scroll = false;
    }

    if( $triggerSection && animate_on_scroll ){

      var posY = globals.scrollTop - $triggerSection.position().top;

			//console.log('posy: ' + posY);

      var _startPosY = $triggerSection.offset().top;

      var $scrollElements = $triggerSection.find('.scroll-point__scroll');
      var _count = $scrollElements.length + 1;
      var _count = 1;

      var _allowNegative = false;

      var animation = $triggerSection.attr('start-animation') != undefined ? $triggerSection.attr('start-animation') : false;

      if(animation && !animating){
        animating = true;
        startAnimation( animation );
      }


      $scrollElements.each(function(){
        var $this = $(this);
        _count++;

        var offset = $this.attr('offset') ? parseInt($this.attr('offset')) : 0;
        var rotate = $this.attr('rotate') != undefined ? true : false;

        // console.log('offset: ', offset, ', rotate: ', rotate);

        if ( offset.toString().indexOf('%') >= 0 ){
          offset = offset.split('%')[0];
          offset = $this.height() * (offset/100);
        }

        // _allowNegative = ($this.hasClass('image-100') || $this.attr('let-it-go') != undefined) ? true : false;
        _allowNegative = ($this.attr('let-it-go') != undefined) ? true : false;


        // var _scrollPosY = 100 - (posY*(_count)) ;
        var _scrollPosY = ((_count*100) + offset) - (posY ) ;

            _scrollPosY = ( _scrollPosY <= 0 && !_allowNegative ) ? 0 : _scrollPosY;
            _scrollPosY = ( _scrollPosY <= -1600 && _allowNegative ) ? (-1600) : _scrollPosY;

						// console.log('_scrollPosY:' + _scrollPosY + ', globals.windowHeight: ' + globals.windowHeight);


        if( $this.hasClass('zoom-in') ){

          var _scale = (1 + (posY - _scrollPosY + (globals.windowHeight/1) )/20000 );
          //console.log('denna ska zoomas: ' + _scale );

          _scale = (_scale > 1) ? _scale : 1;
          _scale = (_scale > 1.05) ? 1.05 : _scale;

          if( $this.hasClass('rotate') ){

            var _spin = posY/50 ;
            _spin = (_spin > 0) ? _spin : 0;

            $this.css({
              'transform': 'scale(' + _scale + ') translate(' + _spin + 'px, ' + _spin + 'px)'
            });
          }
          else {

            $this.css({
              'transform': 'scale(' + _scale + ')'
            });
          }
        }
        else {

          var _opacity = ( _scrollPosY < globals.windowHeight/2 ) ? (1.2 - (_scrollPosY/300)) : 0.6;

  						_opacity = (_opacity < 0.6) ? 0.6 : _opacity;

              _opacity = ($this.hasClass('visible')) ? 1 : _opacity;


          var _rotation = (rotate  ) ? (1.2 - (_scrollPosY/300))*-5 : 0;

          if( $this.attr('horizontal') != undefined ){

            var posX = _scrollPosY/8;
                posX = ( $this.attr('reverse') != undefined ) ? posX*-1 : posX;

            $this.css({
              'transform': 'translate3d(' + posX + 'px, 0, 0) rotateZ(' + _rotation + 'deg)',
              'opacity': _opacity
            })
          }

          else {

            $this.css({
              'display':'block',
              'transform': 'translate3d(0, ' + parseInt(_scrollPosY/5) + 'px, 0) rotateZ(' + _rotation + 'deg)',
              // 'transform': 'translate(0, ' + parseInt(_scrollPosY/5) + 'px) translateZ(0) rotateZ(' + _rotation + 'deg)',
              // 'margin-bottom': (_scrollPosY/5)*-1 + 'px',
              // 'transition':'all 0.2s linear 0s'
              'opacity': _opacity
            })
          }
        }

      });

    }

  }

};




window.hotspots = {
  list: $('.hotspots__list'),

  init: function(){

    hotspots.setEvents();

    $('.product-spec').each(function() {
      var $img = $(this).find('.features__hotspots__image');
      var imgH = $img.height();

      if( imgH > 50 ){

        var $hotspots = $(this).find('.hotspots');

        $hotspots.css({
          'height': imgH,
          'max-height': imgH
        })
      }
      else {
        setTimeout(function () {
          var $hotspots = $(this).find('.hotspots');

          $hotspots.css({
            'height': imgH,
            'max-height': imgH
          })
        }, 1000);
      }


    });



    hotspots.list.each(function(){

      var $hotspots = $(this).find('.hotspots__list__hotspot');

      $hotspots.each(function(){
        var $this = $(this),
            pos = $this.attr('data-pos'),
            posX = pos.split(',')[0],
            posY = pos.split(',')[1];

            //console.log('posX: ' + posX + ', posY: ' + posY );

        $this.css({
          'left': posX + "%",
          'top': posY + "%"
        });
      });

    });
  },

  setEvents: function(){

    $('.flip-toggle').on('click', function(){
      // Works with only two images
      $(this).parent().find('.flip-features__content').toggleClass('active');
      // var $parent = $(this).parent(),
      //     $firstType = $parent.find('.flip-features__content:first-of-type');
      //     $currActive = $parent.find('.flip-features__content.active');
      //
      // $currActive.removeClass('active');
      //
      //
      // if( $currActive.next().hasClass('flip-features__content') ){
      //   $currActive.next().addClass('active');
      // }
      // else {
      //   $firstType.addClass('active');
      //
      // }
    });

    $('[control-spot]').on('mouseover click', function(){
        var _id = $(this).attr('control-spot');
        _id = ( _id.indexOf("#") >= 0 ) ? _id : '#' + _id;

        console.log('spots: ' + _id);

        if( !$(_id).hasClass('show-me') ){
          hotspots.controlSpots(_id);
        }

    });

    $('[control-spots]').on('click', function(){
        var _id = $(this).attr('control-spots');
        _id = ( _id.indexOf("#") >= 0 ) ? _id : '#' + _id;

        if( $(_id).hasClass('show-me') ){
          $(_id).removeClass('show-me');

        }
        else {
          hotspots.controlSpots(_id);
        }

    });

  },

  controlSpots: function(_id){

    var $hotspots = $('.hotspots__list__hotspot');


    $hotspots.removeClass('show-me');

    $(_id).addClass('show-me');

  },

  flashHotspots: function(_hotspotIDs){
    hotspots.controlSpots(_hotspotIDs);

    setTimeout(function () {
      hotspots.controlSpots('');

    }, 1000);
  }
}


window.deepify = {
  wrapper: $('.deepify:not(.parallax):not(.zoom) .deepify__wrapper'),
  parallax: $('.deepify.parallax .deepify__wrapper'),
  zoom: $('.deepify.zoom .deepify__wrapper'),
  layers: $('.deepify__layer'),

  init: function(){

    deepify.setMovement();
    deepify.setParallax();
    deepify.zoomIn();

    //console.log(deepify.parallax);
  },

  setMovement: function(){
    deepify.wrapper.on('mousemove', function(e){

      var $movementLayers = $(this).find('.deepify__layer');

      var posX = (globals.windowWidth/2) - e.pageX;
      var posY = (globals.windowHeight/2) - e.pageY + globals.scrollTop;

      // console.log(posX);

      var moveDiff = ($movementLayers.length* ($movementLayers.length/4) ) /2000;

      var _x = posX * moveDiff;
      var _y = posY * moveDiff;

      var _count = 1;

      $movementLayers.each(function(){

         _count+=1;

        var _scaleX = 1;
        var _scaleY = 1;

        if(posX > 0){
          _scaleX = 1 + _x/1000;
        }
        else {
          _scaleX = 1;
        }

        if(posY > 0){
          _scaleY = 1 + _y/1000;
        }
        else {
          _scaleY = 1;
        }


        $(this).css({
          'perspective': $(this).width(),
          'transform': 'translate3d(' + _x*1*(_count-2) + 'px, ' + _y*(_count-2) + 'px, 0) scaleX(' + _scaleX + ') scaleY(' + _scaleY + ')'
        });

      });


    });
  },

  setParallax: function(){
    $(window).on('scroll', function(e){

      // var posY = (globals.windowHeight/2) - globals.scrollTop;
      var posY = globals.scrollTop;

      //console.log(posY);

      $('.deepify.parallax').each(function(){

        //console.log('parallax: ', this);

        if( posY >= $(this).position().top ){

          var _dir = ($(this).attr('reverse') != undefined ) ? -1 : 1;

          // var $parallaxLayers = deepify.parallax.find('.deepify__layer');
          var $parallaxLayers = $(this).find('.deepify__layer');
          var moveDiff = $parallaxLayers.length* ( $parallaxLayers.length ) /500;
          var _y = posY * moveDiff;
          var _count = 0;

          $parallaxLayers.each(function(){

    				posY = globals.scrollTop - $(this).position().top;

             _count+=1;

            var _scaleY = 1;


            if(posY > 0){
              _scaleY = 1 + _y/1000;
            }
            else {
              _scaleY = 1;
            }

            var dY = 2*(_y*(_count-2) * _dir);

            // dY = ( dY < -200 ) ? -200 : dY;

            $(this).css({
              'perspective': $(this).width(),
              'transform': 'translate3d(0, ' + dY + 'px,0) scaleY(' + _scaleY + ')'

            });
          });
        }

      });


    });
  },

  zoomIn: function(){
    $(window).on('scroll', function(e){

      // var posY = (globals.windowHeight/2) - globals.scrollTop;
      var posY = globals.scrollTop;

      //console.log(posY);

      var $parallaxLayers = deepify.zoom.find('.deepify__layer');

      var moveDiff = $parallaxLayers.length* ( $parallaxLayers.length ) /4000;

      var _y = posY * moveDiff;

      var _count = 0;

      // deepify.layers.each(function(){
      $parallaxLayers.each(function(){

				posY = globals.scrollTop - $(this).position().top;

         _count+=1;

        var _scaleY = 1;


        if(posY > 0){
          _scaleY = 1 + _y/(50+_count);
        }
        else {
          _scaleY = 1;
        }

        //console.log('parallax px: ' + parseInt( _y*(_count-2)));

        $(this).css({
          'perspective': $(this).width(),
          // 'transform': 'translate3d(0, ' + _y*(_count-2) + 'px,0) scale(' + _scaleY + ')'
          'transform': 'translate3d(0, -' + (_y/1)*(_count-2) + 'px,0) scale(' + _scaleY + ')'


          // 'margin-top': parseInt( _y*(_count-2)) + 'px'
        });
      });

			$('.scroll-point__fullsize__bg').css({
				'transform': 'scale(' + globals.scrollTop/70 + ')'
        // 'transform': 'scale(' + ($(this).position().top - globals.scrollTop/70) + ')'

			});


    });
  }

}

window.banner = {
  element: $('[class*="banner"] img:not(.no-animation)'),
  object: $('.article .container'),

  animateOnScroll: function(){

    banner.element.each(function(){

      $(this).css({
        'min-height': '100%'
      })

      var posY = ( $(window).scrollTop() - $(this).offset().top )/10;

      // if( globals.windowWidth < 768 ){
      //   posY += 40;
      // }

      // posY = (posY > 0)? posY :0;

      $(this).css({
        'transform': 'translateY(' + posY + 'px)'
      });

    });

    // var posY = $(window).scrollTop()/10;
    //
    // banner.element.css({
    //   'transform': 'translateY(' + posY + 'px)'
    // });

    // if(posY < 45 && banner.element.length > 0){
    //   banner.object.css({
    //     'transform': 'translateY(' + posY/1.5 + 'px)'
    //   });
    // }
  }
}



window.showNhide = {
  elements: $('[show-n-hide]'),
  triggers: $('[show-n-hide-trigger]'),

  init: function(){

    showNhide.elements.each(function(){

      var $this = $(this),
          _showThis = $this.attr('show-n-hide');

          $this.find('[show-n-hide-trigger]').each(function(){
            var $trigger = $(this),
                _triggerThisID = $(this).attr('show-n-hide-trigger');
            // console.log('show-n-hide-trigger: ' + _triggerThisID);

            $trigger.on('click', function(){
              $this.attr('show-n-hide', _triggerThisID);
              showNhide.runFunction(_triggerThisID);
            });
          });

      showNhide.runFunction(_showThis);

      $this.find('img[show-n-hide-id]').on('click', function(){

        if( $this.find('.color-list .active').next().length > 0 ){
          $this.find('.color-list .active').next().trigger('click');
        }
        else {
          $this.find('.color-list li:first-of-type').trigger('click');
        }

      });

    });



  },

  runFunction: function( _showThis ){
    // console.log(_showThis.split(','));
    var _vars = _showThis.split(',');

    var parentStr = "";
    var qString = "";

    if ( _vars.length > 1 ){

      for (var i = 0; i < _vars.length; i++) {
        // console.log('qString.length: ' + qString.length);
        var _comma = (qString.length > 0) ? ',' : '';
        qString += (_comma +_vars[i]);
        // parentStr += '[show-n-hide="' + qString + '"] ';

        if(i == 0){

          $( '[show-n-hide="' + _showThis + '"] [show-n-hide-trigger]').removeClass('active');
          $( '[show-n-hide="' + _showThis + '"] [show-n-hide-id]').removeClass('show-me');


          parentStr += '[show-n-hide="' + _showThis + '"] ';

          $( parentStr + '[show-n-hide-id="' + _vars[i] + '"]').addClass('show-me');
          $( parentStr + '[show-n-hide-trigger="' + _vars[i] + '"]').addClass('active');


        }
        else {

          parentStr += '[show-n-hide-id="' + _vars[i] + '"] ';
          $( parentStr ).addClass('show-me');
          // Denna funkar med "rubriker" herr/dam
          // $( '[show-n-hide="' + _showThis + '"] [show-n-hide-trigger*="' + _vars[0] + '"]').addClass('active');
          $( '[show-n-hide="' + _showThis + '"] [show-n-hide-trigger="' + _showThis + '"]').addClass('active');
          // console.log('*här är vi nu, parentstr: ' + parentStr + ', _showThis: '+ _showThis);

        }

      }
    }
    else {
      $( '[show-n-hide="' + _showThis + '"] [show-n-hide-trigger]').removeClass('active');
      $( '[show-n-hide="' + _showThis + '"] [show-n-hide-id]').removeClass('show-me');

      $('[show-n-hide="' + _showThis + '"] [show-n-hide-id="' + _showThis + '"]').addClass('show-me');
      $('[show-n-hide="' + _showThis + '"] [show-n-hide-id="' + _showThis + '"] [show-n-hide-id]:first-of-type').addClass('show-me');
      $('[show-n-hide="' + _showThis + '"] [show-n-hide-trigger="' + _showThis + '"]').addClass('active');
      // $('[show-n-hide="' + _showThis + '"] [show-n-hide-trigger="' + _showThis + '"]').addClass('active');

    }

    // parentStr += '[show-n-hide="' + qString + '"] ';

    // $( '[show-n-hide-id="' + _showThis + '"]').addClass('show-me');


    // console.log('parentStr: ' + parentStr);

    // $( parentStr + '[show-n-hide-trigger="' + _showThis + '"]').addClass('active');

    // $( parentStr + '[show-n-hide-id="' + _vars[ _vars.length - 1] + '"]').addClass('show-me');

    if ( _vars.length > 1){


    }
    else {

      // $('[show-n-hide="' + _showThis + '"] [show-n-hide-trigger]').removeClass('active');
      // $('[show-n-hide="' + _showThis + '"] [show-n-hide-trigger="' + _showThis + '"]').addClass('active');
      //
      // $('[show-n-hide="' + _showThis + '"] [show-n-hide-id]').removeClass('show-me');
      // $('[show-n-hide="' + _showThis + '"] [show-n-hide-id="' + _showThis + '"]').addClass('show-me');
    }

  }
}



$(document).ready(function(){
  navigation.init();
  // showOnScroll.init();
  //showOnScroll.render();

  scrollPoints.render();


  hotspots.init();

	deepify.init();

  mouse.init();

  showNhide.init();

  globals.orientation();



  // $('.hotspots').on('click', function(e){
  //   console.log('klick');
  //   console.log('spots: ', $(this).parent().find('[id*=spot]'));
  //
  //   hotspots.flashHotspots('#spot1, #spot2, #spot3, #spot4');
  // });

  $('body').addClass('page-loaded');


});

$(window).on('scroll', function() {
  // showOnScroll.render();
  globals.updateScrollPos();

  mirror.render();
  scrollPoints.render();
	banner.animateOnScroll();


});

$(window).on('resize', function() {
  scrollPoints.calculateVariables();

  globals.orientation();

});
