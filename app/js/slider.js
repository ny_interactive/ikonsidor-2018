
window.sliders = {
	slider: {},
	
	init: function(){
		this.setupBannerSlider();
	},
	setupBannerSlider : function(){
		this.slider = $(".slider");

		if(this.slider.length > 0){

			this.slider.slick({
				autoplay: false,
				dots:true,
				prevArrow: '<div class="slick-arrow slick-prev"></div>',
				nextArrow: '<div class="slick-arrow slick-next"></div>'
			})
			.on('beforeChange', function(event, slick, currentSlide, nextSlide){

			  	//console.log('currentSlide: ',currentSlide, 'nextSlide: ', nextSlide);
			  	$('.slider-nav a').removeClass('active');

				//console.log('DENNA SKA VARA AKTIV: ', $('a[data-slide=' + (nextSlide) + ']'));

				$('a[data-slide=' + (nextSlide) + ']').addClass('active');

			});
		}

		$('.slick-arrow').on('click', function(){
			var $movie_wrapper = $('.movie-wrapper');

	     	movie.endMovie($movie_wrapper);
		});

		this.setupMultipleSlider();
	},

	setupMultipleSlider : function(){
		$('.multiple-slider').slick({
		  centerMode: true,
		  infinite:true,
		  slidesToShow: 5,
		  slidesToScroll: 3,
		  prevArrow: '<div class="slick-arrow slick-prev"></div>',
		  nextArrow: '<div class="slick-arrow slick-next"></div>',
		  responsive: [
		    {
		      breakpoint: 768,
		      settings: {
		        arrows: true,
		        centerMode: true,
		        centerPadding: '40px',
		        slidesToShow: 3,
		        slidesToScroll: 3
		      }
		    },
		    {
		      breakpoint: 480,
		      settings: {
		        arrows: true,
		        centerMode: true,
		        centerPadding: '40px',
		        slidesToShow: 1
		      }
		    }
		  ]
		})
		.on('beforeChange', function(event, slick, currentSlide, nextSlide){

		});

	}

}