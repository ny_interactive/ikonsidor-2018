
/**

 * * * * *  *  *   *   *
 *   *   *     *   *   *
 *   *   *     * * * * *


 * MAW
 *
 * @description Feadback tool with notes
 * @author      Daniel Melkersson
 * @version     0.9.1
 * @date        2018-01-10
 */

$.getScript("https://www.gstatic.com/firebasejs/4.8.1/firebase.js", function() {

   // Initialize Firebase
     var config = {
       apiKey: "AIzaSyB_mMmypicAai6-oD7ovLqmWkB9vMOF76c",
       authDomain: "feedback-notes.firebaseapp.com",
       databaseURL: "https://feedback-notes.firebaseio.com",
       projectId: "feedback-notes",
       storageBucket: "feedback-notes.appspot.com",
       messagingSenderId: "186639496443"
     };
     firebase.initializeApp(config);


   // Use the shorthand notation to retrieve the default app's services
   var fbStorage = firebase.storage();
   var fbDatabase = firebase.database();
});


var mouseIsDown = false;


window.notes = {
  active:false,
  host: window.location.hostname,
  path: window.location.pathname,
  users:{},
  hoveringThisID: "",
  currentUser: {
    id: "",
    name: "",
    color: ""
  },
  area: "",
  // colors:["#ada396", "#b4e7a0", "#e3eaa7", "#d6e1df", "#eca1a6", "#bdcebe", "#d6ccd3", "#87af49"],
  colors:["#a5cdbe", "#cdf0e1", "#c8cccc", "#ffd982", "#d0bf80", "#dfe0d6", "#afc8dc", "#c2e2e8", "#bec8d2", "#ffc8c9", "#fce3df", "#e6d7d7"],
  sizes: {
    mobile: {
      ww: 375,
      wh:667,
      ua: 'Mozilla/5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1',
      av: "5.0 (iPhone; CPU iPhone OS 10_3 like Mac OS X) AppleWebKit/602.1.50 (KHTML, like Gecko) CriOS/56.0.2924.75 Mobile/14E5239e Safari/602.1"

    },
    portrait: {
      ww: 768,
      wh:1024
    },
    desktop: {
      ww: 1200,
      wh:1024
    }
  },
  docHeight: $(document).height(),
  winHeight: $(window).height(),
  docWidth: $(document).width(),
  mouse:{ x: 0, y: 0, move:false },
  device: "desktop",

  init: function(){

    // document.documentElement.style.setProperty(`width`, '400px');

    var timer = setInterval(function () {

      if(firebase){
        console.log('ready to rock');
        notes.setUp();
        clearInterval(timer);
      }
    }, 100);



  },

  setUp: function(){
    // $(document).css({
    //  'width': '400px',
    //  'margin': 'auto'
    // })

    notes.handleUser();

    notes.host = notes.host.replace(/[|&;$%@"<>()+,.]/g, "");

    console.log('host: ' + notes.host);

    var $toggle = $('<div class="toggle-notes" available-notes="0" />');

    var $mobile = $('<div id="notes-mobile" class="notes-device" />'),
        $portrait = $('<div id="notes-portrait" class="notes-device" />'),
        $desktop = $('<div id="notes-desktop" class="notes-device" />');

        $toggle.append($mobile,$portrait,$desktop);

    notes.area = $('<div class="note-area" />');


    setTimeout(function () {
      notes.setHeight();

    }, 1000);


    this.getMousePos();


    $('body')
      .append(notes.area)
      .append($toggle);

    notes.device = notes.getDevice();

    var timer = setInterval(function () {

      if(notes.currentUser.id.length > 2){
        console.log('ready to roll');
        notes.render();
        clearInterval(timer);
      }
    }, 100);

  },

  render: function(){
    this.getNotes();
    this.setEvents();
  },

  setSize:function(_ww, _wh, _uA, _aV){
    _uA = notes.sizes.mobile.ua;
    _aV = notes.sizes.mobile.av;

    $('body').css({
      'min-width' : _ww,
      'margin' : 'auto'
    });

    notes.area.css({
      'width': _ww,
      'width': '100%',
      'margin' : 'auto'

    })
  },

  getDevice: function(){
    var _device = $('.notes-device:visible').attr('id').split('notes-')[1];
    console.log('device: ' + _device);
    return _device;
  },

  getBrowser: function() {
    var browsers = [
        {match: 'Chrome', name: 'Chrome'},
        {match: 'MSIE 9', name: 'IE 9'},
        {match: 'MSIE 10', name: 'IE 10'},
        {match: 'Safari', name: 'Safari'},
        {match: 'Firefox', name: 'Firefox'}
    ];

    var ua = navigator.userAgent;
    var profile, re;

    for (var i=0, iLen=browsers.length; i<iLen; i++) {
      profile = browsers[i];

      if (ua.match(profile.match)) {
        return profile.name;
      }
    }
  },

  setUserAgent: function(window, userAgent, appVersion) {
      // Works on Firefox, Chrome, Opera and IE9+

      Object.defineProperty(window.navigator, 'appVersion', {
        value: appVersion,
        writable: true
      });

      Object.defineProperty(window.navigator, 'userAgent', {
        value: userAgent,
        writable: true
      });

  },

  setHeight:function(){

    notes.docHeight = $(document).height();
    notes.winHeight = $(window).height();

    notes.area.css({
      'height': notes.docHeight
    });

  },

  getRandomColor: function(){
    return notes.colors[ Math.floor( Math.random() * notes.colors.length ) ];
  },

  handleUser: function(){

    if( localStorage.getItem("notes-userInfo") ){
      notes.currentUser = JSON.parse( localStorage.getItem("notes-userInfo") );
      notes.currentUser.auth = (notes.currentUser.auth) ? notes.currentUser.auth : "normal";
      $('body').addClass(notes.currentUser.auth);

    }
    else {
      notes.getUserInfo();
    }


    var _refUsers = firebase.database().ref('users');

    _refUsers.once('value',function(users) {

      var _userIds = Object.keys(users.val());
      var count = 0;

      users.forEach(function(_user) {
        var $user = _user.val();
        var _key = _userIds[count];

        notes.users[_key] = $user;

        count++;

      })

    });

    //$('html').attr('active-user', notes.currentUser.id);


  },

  getUserInfo: function(){

    var $infoWrapper = $('<div class="notes__user-info" />'),
        $infoForm = $('<form class="notes__user-form" />'),
        $currentUsers = $('<select class="current-users" />'),
        $inputFirstName = $('<input class="notes__first-name" type="text" placeholder="Förnamn" />'),
        $inputLastName = $('<input class="notes__last-name" type="text" placeholder="Efternamn" />'),
        $submit = $('<input type="submit" value="Ok"/>');

    var _refUsers = firebase.database().ref('users');

    $currentUsers.append('<option value="-" >Välj befintlig användare</option>');

    $currentUsers.on('change', function(){
      var _value = $(this).val();

      if(_value != '-'){
        $infoWrapper.addClass('user-chosen');
      }
      else {
        $infoWrapper.removeClass('user-chosen');
      }

    });

    var _allUsers;

    _refUsers.once('value',function(users) {
      _allUsers = users.val();

      var _userIds = Object.keys(users.val());
      var count = 0;

      users.forEach(function(_user) {
        var $user = _user.val();
        var _key = _userIds[count];

        $currentUsers.append('<option value="' + _key + '" >' + $user.name +'</option>');
        count++;
      })
    });


    $infoForm.append(
      $currentUsers,
      '<hr>',
      $inputFirstName,
      $inputLastName,
      $submit
    );

    $infoWrapper.append(
      $infoForm
    );

    $infoForm.on('submit', function(e){

      e.preventDefault();

      var _name = $inputFirstName.val() + ' ' + $inputLastName.val(),
          _initials = $inputFirstName.val().substring(0,1) + $inputLastName.val().substring(0,1),
          _id = notes.getRandomId(),
          _auth = 'normal',
          _color = notes.getRandomColor();

          _name = ( _name.length > 2 ) ? _name : "No Name";
          _initials = ( _initials.length > 1 ) ? _initials : "NN";


          if( $currentUsers.val() != '-' ){
            _id = $currentUsers.val();
            _color = _allUsers[ _id ].color;
            _name = _allUsers[ _id ].name;
            _initials = _allUsers[ _id ].initials;
            _auth = (_allUsers[ _id ].auth != undefined) ? _allUsers[ _id ].auth : 'normal';
          }
          else {
            notes.saveUser(_id, _name, _initials, _color, _auth );
          }

      notes.currentUser.id = _id;
      notes.currentUser.name = _name;
      notes.currentUser.initials = _initials;
      notes.currentUser.color = _color;
      notes.currentUser.auth = _auth;

      // console.log('notes.currentUser: ', notes.currentUser);

      localStorage.setItem("notes-userInfo", JSON.stringify( notes.currentUser ) );
      $('body').addClass(notes.currentUser.auth);

      $infoWrapper.remove();
    });

    $('body').append($infoWrapper);




    // localStorage.setItem("notes-userID", _id);
    // localStorage.setItem("notes-userName", _name);

  },

  getCurrentUser: function(){

    return notes.currentUser.id;

  },

  getUserName: function(_id){
    return notes.currentUser.initials;
  },

  getUserColor: function(_id){
    return notes.currentUser.color;
  },

  saveUser: function( _id, _name, _initials, _color, _auth){

    firebase.database().ref('users').child( _id ).set({
      name: _name,
      initials: _initials,
      color: _color,
      auth: _auth
    });
  },

  getNotes: function(){
    // console.log('GET NOTES'+ notes.host + notes.path);

    // console.log('path: ' + notes.path + ', length: ' + notes.path.length);
    var _refNotes;

    console.log('notes.users: ', notes.users);

    if( notes.path.length > 1 ){
      _refNotes = firebase.database().ref('notes').child(notes.host).child(notes.path);
    }
    else {
      _refNotes = firebase.database().ref('notes').child(notes.host);
    }

    var keys={};

    _refNotes.once('value',function(snap) {

      var _count = 0;

      // if( '/' + snap.key + '/' == notes.path ){
        snap.forEach(function(item) {
          // console.log('item: ', item.key);
            var itemVal = item.val();

            keys[item.key] = itemVal;

            // console.log("itemVal: " , itemVal);

            if( item.key.search('note_') == 0 ) {




              // var _userName = notes.getUserName(itemVal.userID);
              var _userName, _userColor;

              var ref = firebase.database().ref("users/" + itemVal.userID);
                ref.once("value")
                  .then(function(snapshot) {
                    // var _userName = snapshot.child("name").val();
                    _userName = snapshot.child("initials").val();
                    _userColor = snapshot.child("color").val();

                    var _auth = (snapshot.child("auth").val()) ? snapshot.child("auth").val() : "normal";

                    // console.log("notes.currentUser.auth: " + notes.currentUser.auth);
                    // console.log("_auth: " + _auth );

                    if( (notes.currentUser.auth == "read-all") || ( notes.currentUser.auth == "normal" && _auth != "read-all" ) ){

                      if (itemVal.status != 'done'){
                        _count++;
                      }

                      var $note = notes.addNote(item.key, itemVal.userID, itemVal.posX , itemVal.posY ,itemVal.text, itemVal.device, itemVal.ww, itemVal.wh, itemVal.date, _userName, _userColor, itemVal.status, _auth );

                      if( itemVal.comments ){

                        var _keys = Object.keys(itemVal.comments);
                        var _comments = Object.values(itemVal.comments);

                        // users.forEach(function(_user) {
                        //   var $user = _user.val();
                        //   var _key = _userIds[count];
                        //
                        //   $currentUsers.append('<option value="' + _key + '" >' + $user.name +'</option>');
                        //   count++;
                        // })

                        // $( _comments ).each(function(_key, _val){
                        _comments.forEach(function(_val, _key){

                          var _comment = _val;

                          // var commentRef = firebase.database().ref("users/" + _val.userId);
                          //   commentRef.once("value")
                          //     .then(function(_user) {


                          // var $user =  _user.val();
                          var $user =  notes.users[_val.userId];
                          $user.id = _comment.userId;



                          //_commentID, $note, $user, _date, _commentText
                          notes.addComment( _keys[_key], $note, $user, _comment.date, _comment.text);


                            // });

                        });


                      }
                    }


                    if( _count >= 0 ){
                      $('.toggle-notes').attr('available-notes', _count);
                    }
                  });

            }

        });

        // console.log('available notes: ', Object.keys(keys).length );
        // console.log('available notes: ', _count );

        // if( _count > 0 ){
        //   console.log('nu skrivs värdet count ut som: ' + _count);
        //   $('.toggle-notes').attr('available-notes', _count);
        //   window.innerWidth =600;
        //   // var myWindow = window.open("", "", "width=100, height=100");
        //   // myWindow.resizeTo(250, 250);
        //   // myWindow.focus();
        // }

      // }



    });

    //$('.note[user="' + notes.currentUser.id + '"]').addClass('editable');


  },

  updateAvailableNotes: function( _diff ){

    var _count = parseInt( $('.toggle-notes').attr('available-notes') );
    // console.log('count: ' + _count);
    // _count = (_count != undefined && _count != NaN && !_count) ? _count : 0;
    // console.log('count: ' + _count);

    $('.toggle-notes').attr('available-notes', _count + _diff);

  },

  setEvents:function(){

    $('.toggle-notes').on('click', function(e){
      if($(e.target).hasClass('toggle-notes')){
        $('body').removeClass('notes-on');
        notes.setSize(notes.docWidth,'auto');

      }
    });

    var developerTrigger = 0;

    $(document).keyup(function(e) {


      switch (e.keyCode){

        // Enter
        case 13:
          developerTrigger++;

          if(developerTrigger > 2){
            $('html').addClass('developer');
          }

          break;

        // Esc
        case 27:
          $('body').removeClass('notes-on');
          notes.setSize(notes.docWidth,'auto');

            developerTrigger = 0;

          break;

        default:
          developerTrigger = 0;

          break;

      }
    });



    $('.notes-device').on('click', function(){
      $('body').addClass('notes-on');
      var _id = $(this).attr('id').split('notes-')[1];
      var _ww = notes.docWidth,
          _wh = notes.docHeight;
      // var _ww = notes.sizes[_id].ww,
      //     _wh = notes.sizes[_id].wh;
      notes.setSize(_ww,_wh);
    });

    $('.note-area').on('click', function(e){

      if ( $(e.target).hasClass('note-area') && !notes.mouse.move ){
        // var mouseY = notes.mouse.y;
        var mouseY = e.offsetY;
        // var mouseX = notes.mouse.x;
        var mouseX = e.offsetX;
        mouseX = ( (mouseX + 200) > notes.docWidth ) ? (notes.mouse.x - 200) : notes.mouse.x;
        var _id = 'note_' + notes.getRandomId();
        var percentX = parseInt( (mouseX/notes.docWidth)*100 );
        var percentY = parseInt( (mouseY/notes.docHeight)*100 );
        var _text = "";

        var _userID = notes.getCurrentUser();

        var _name = notes.getUserName();

        notes.addNote(_id, _userID, percentX, percentY,_text, notes.device, notes.docWidth, notes.docHeight, notes.getTheDate(), _name, notes.currentUser.color, 'pending', false);
        notes.updateAvailableNotes(1);
        notes.saveNote(_id, _text, percentX, percentY, _userID);

      }

    });

  },
  getMousePos: function(){

    $('body').on('mousemove', function(e){

      notes.mouse.x = e.pageX;
      notes.mouse.y = e.pageY;
      // notes.mouse.y = e.pageY + $(this).scrollTop();
      // console.log('st: ' + $(this).scrollTop() + ' , mouse: ' + notes.mouse.x + ', ' +  notes.mouse.y);
    })

  },
  addNote: function(_id, _userID, percentX, percentY, _text, _device, _ww, _wh, _date, _initials, _userColor, _status, _auth){


    var editable = (notes.currentUser.id == _userID) ? true : false;

    var editableClass = (editable) ? 'editable' : '';

    var $note = $('<div id="' + _id + '" user="' + _userID + '" class="'+ _status +' ' + editableClass + ' note note-on-' + _device + '" />'),
        $date = $('<span class="note__date" />'),
        $name = $('<span class="note__name" />'),
        $textArea = $('<textarea />'),
        $commentBtn = $('<div class="add-comment" />'),
        $deleteBtn = $('<div class="delete-note" />'),
        $doneBtn = $('<div class="done-note" />');

    _userColor = (!_userColor) ? notes.getRandomColor() : _userColor;


    $date.html(_date);
    $name.html(_initials).css('background',_userColor);
    $note.css({
      'border-color': _userColor,
      'background': _userColor
    });

console.log('_auth: ', _auth);

    if(_auth == "read-all"){
      $note.addClass('inhouse');
    }

    $textArea.val(_text);

    var _originalText = _text;



    $deleteBtn.on('click', function(){
      notes.deleteNote( $note );
    });

    $doneBtn.on('click', function(){
      notes.markAsDone( $note );
    });

    $commentBtn.on('click', function(){
      var _commentID = notes.saveComment(_id, notes.currentUser.id);

      console.log('commentID: ' + _commentID);
      notes.addComment( _commentID, $note, notes.currentUser, notes.getTheDate(), "" );
    });

    $textArea.on('keyup', function(e){

      // var _edited = false;
      //
      // if( !editable ){
      //   $note.addClass('edited-by-someone');
      //   _edited = true;
      //
      // }

      if(editable){
        _text = $textArea.val();

        notes.updateNote(_id, _text );
      }
      else {

        notes.addMessage($note, 'Du kan inte redigera någon annans text. För att kommentera texten så klickar du på pratbubblan längst ner på varje notis.');

        $textArea.val(_text);
      }


    });

    notes.makeDraggable($note, _id);


    // $note.append($date, $name, $textArea, $doneBtn);
    // $note.append($date, $name, $textArea, $deleteBtn);
    $note.append($date, $name, $textArea, $deleteBtn, $doneBtn, $commentBtn);

    $note.css({
      'top': percentY + "%",
      'left': percentX + "%"
    });

    notes.area.append($note);


    setTimeout(function(){
      $note.addClass('stuck');
      $textArea.focus();

    },.10);

    return $note;

  },

  addMessage: function($note, _message, _time){
    _time = (_time)? _time : 2500;
    var $message = $('<div class="note-message" />'),
        $confirmBtn = $('<div class="note-message__confirm" />');

    $confirmBtn.on('click', function(){
      setTimeout(function () {
        $message.removeClass('show-message');
        $note.removeClass('do-not-blur');
      }, (100));
      setTimeout(function () {
        notes.area.removeClass('blur');
      }, (500));
      setTimeout(function () {
        $message.remove();
      }, (1000));
    });

    if ( !notes.area.hasClass('blur') ){

      notes.area.addClass('blur');
      // $note.addClass('do-not-blur');

      $message.html(_message);
      $message.append($confirmBtn );

      // $note.append($message);
      $note.after($message);

      setTimeout(function () {
        $message.addClass('show-message');
      }, 10);
    }

    // setTimeout(function () {
    //   $message.removeClass('show-message');
    //   $note.removeClass('do-not-blur');
    //   notes.area.removeClass('blur');
    //
    //
    // }, (_time));
    // setTimeout(function () {
    //   // $note.remove($message);
    //   $message.remove();
    //
    // }, (_time + 500));

  },

  deleteNote: function( $note ){

    var _id = $note.attr('id');

    firebase.database().ref('notes').child(notes.host).child(notes.path).child(_id).remove();

    $note.removeClass('stuck');
    notes.updateAvailableNotes(-1);


    setTimeout(function () {
      $note.remove();
    }, 600);

  },
  deleteComment: function( $note, $comment ){

    var _noteID = $note.attr('id');

    var _id = $comment.attr('id');

    firebase.database().ref('notes').child(notes.host).child(notes.path).child(_noteID).child('comments').child(_id).remove();

    $comment.remove();

  },
  markAsDone: function( $note ){

    var _id = $note.attr('id');

    firebase.database().ref('notes').child(notes.host).child(notes.path).child(_id).update({
      status: 'done'
    });

    // $note.removeClass('stuck');
    notes.updateAvailableNotes(-1);


    setTimeout(function () {
      // $note.remove();
      $note.addClass('done');

    }, 600);

  },

  addComment: function( _commentID, $note, $user, _date, _commentText ){

    var _id = $note.attr('id');

    $note.addClass('has-note-comments');

    console.log('$_commentID: ', _commentID);

    $note.attr('before','test')

    var _userColor = $user.color,
        _initials = $user.initials;

    var editable = (notes.currentUser.id == $user.id) ? true : false;

    var editableClass = (editable) ? 'editable' : '';

    var $comment = $('<div class="note__comment ' + editableClass + '" id="' + _commentID + '" user="' + $user.id + '" />'),
    // var $comment = $('<div class="note__comment" />'),
        $name = $('<span class="note__name" />'),
        $deleteBtn = $('<div class="delete-note" />'),
        $date = $('<span class="note__date" />'),
        $text = $('<textarea />');

    $text.val(_commentText);

    $deleteBtn.on('click', function(){
      notes.deleteComment( $note, $comment );
    });

    $date.html( _date );
    $name.html(_initials).css('background',_userColor);

    $comment.append($name, $date, $text, $deleteBtn);

    $comment.css({
      'border-color': _userColor,
      'background': _userColor
    });


      $text.on('keyup', function(e){
        if(editable){
          _commentText = $text.val();
          notes.updateComment(_id, _commentText, _commentID );
        }
        else {

          notes.addMessage($note, 'Du kan inte redigera någon annans text. För att kommentera texten så klickar du på pratbubblan längst ner på varje notis.');

          $text.val(_commentText);
        }
      });



    $note.append($comment);

    setTimeout(function(){
      $note.addClass('note-commented');
      $text.focus();

    },.10);



  },

  saveComment: function( _noteID, _userID ){

    // var commentRef = firebase.database().ref('notes').child(notes.host).child(notes.path).child(_noteID).child('comments').push();
    var commentRef = firebase.database().ref('notes').child(notes.host).child(notes.path).child(_noteID).child('comments');

    var _countComments = $("#" + _noteID).find('.note__comment').length;

    // _countComments = parseInt( $("#" + _noteID).find('.note__comment:last-of-type').attr('id').split('_')[0] );

    var _commentID = (_countComments + 1) + "_" + notes.getRandomId();

    commentRef.child(_commentID).set({
      userId: _userID,
      text: "",
      date: notes.getTheDate()
    });

    return _commentID;

  },

  updateComment: function(_id, _text, _commentId){

    firebase.database().ref('notes').child(notes.host).child(notes.path).child(_id).child('comments').child( _commentId ).update({
      text: _text,
    });

  },
  getRandomId: function(){
    return Math.random().toString(36).substr(2, 9);
  },

  getTheDate: function(){

    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) {
        dd = '0'+dd
    }

    if(mm<10) {
        mm = '0'+mm
    }

    today = yyyy + '-' + mm + '-' + dd;

    return today;

  },

  saveNote: function(_id, _text, _x, _y, _userID){

    firebase.database().ref('notes').child(notes.host).child(notes.path).child(_id).set({
      text: _text,
      date: notes.getTheDate(),
      posX: _x,
      posY: _y,
      status: 'pending',
      userID: _userID,
      device: notes.device,
      browser: notes.getBrowser(),
      ww: notes.docWidth,
      wh: notes.winHeight
    });
  },

  updateNote: function(_id, _text){

    firebase.database().ref('notes').child(notes.host).child(notes.path).child(_id).update({
      text: _text
    });

  },

  makeDraggable: function(elmnt, _id){

    var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;

    elmnt.onmousedown = dragMouseDown;

    $(elmnt).on('mousedown', dragMouseDown);


    function dragMouseDown(e) {

      $(elmnt).removeAttr('connected-to');

      e = e || window.event;
      // get the mouse cursor position at startup:
      pos3 = e.clientX;
      pos4 = e.clientY;
      document.onmouseup = closeDragElement;
      // call a function whenever the cursor moves:
      document.onmousemove = elementDrag;
    }

    function elementDrag(e) {
      e = e || window.event;
      // calculate the new cursor position:
      pos1 = pos3 - e.clientX;
      pos2 = pos4 - e.clientY;
      pos3 = e.clientX;
      pos4 = e.clientY;

      notes.mouse.move = true;

      // set the element's new position:
      // elmnt.style.top = (elmnt.offsetTop - pos2) + "px";
      // elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";

      var offTop = $(elmnt).offset().top;
      var offLeft = $(elmnt).offset().left;

      var moveX = (offLeft - pos1 );

      var _noteWidth = $(elmnt).width();

      moveX = ( (moveX + _noteWidth) > notes.docWidth ) ? (notes.docWidth - _noteWidth) : moveX;
      // var moveX = parseInt( ((offLeft - pos1)/notes.docWidth)*100 );
      var moveY = (offTop - pos2 );



      moveY = ( (moveY + _noteWidth) > notes.docHeight ) ? (notes.docHeight - _noteWidth) : moveY;
      // var moveY = parseInt( ((offTop - pos2)/notes.docHeight)*100 );

      $(elmnt).css({
        'transition': 'all 0s linear',
        'top': moveY + "px",
        'left': moveX + "px",
        'z-index': 10

      });

    }



    function closeDragElement() {
      /* stop moving when mouse button is released:*/
      document.onmouseup = null;
      document.onmousemove = null;

      setTimeout(function () {
        notes.mouse.move = false;
      }, 100);

      var _posX = parseInt( ($(elmnt).offset().left/notes.docWidth)*100 );
      var _posY = parseInt( ($(elmnt).offset().top/notes.docHeight)*100 );

      firebase.database().ref('notes').child(notes.host).child(notes.path).child(_id).update({
        posX: _posX,
        posY: _posY
      });

    }



  }

}

notes.init();


$(window).on('orientationchange',function(){
  window.location.reload();
});

$(window).resize(function(){
  notes.docHeight = $(document).height();
  notes.docWidth = $(document).width();

  // setTimeout(function () {
    notes.setHeight();

  // }, 1000);
});

// $.getJSON('//freegeoip.net/json/?callback=?', function(data) {
//   console.log('IP: ', JSON.stringify(data, null, 2));
// });
