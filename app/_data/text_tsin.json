{
  "en": {
    "main_title": "The Season is Now",

    "text_block1": "There is no timetable for new experiences. That great adventure of yours has no solid rules, and no set conditions. And neither does the everyday-ones. Instead it’s all up to you. To walk further, to explore more, and see the changes of the seasons. <br>
Take the opportunity; and go outside. Because, in your mind, you know it really is that simple. Keep that thought, and let it move you into action. Get your backpack, tie your boots and make the most of what’s right in front of you.<br>
The season is now.",

    "one_col_title": "Functionality<br/>& Comfort",
    "one_col_text": "<p>For 85 years, we have seen the seasons change – along with weather, activities, and peoples’ dreams. Having our roots in the demanding mountain region of Jämtland, Sweden, means that we have learnt the importance of having reliable and comfortable boots, clothes, and equipment; and not to mention, we know the value of sustainability.</p><p>When choosing a pair of boots, a piece of clothing or a backpack from Lundhags you can rest assured that it will last both when the seasons change, and over time.</p><p>Our products are made for you to enjoy wearing, and to use for a very long time; a thought-trough collection, made from durable materials, with high repairability, and a Lundhags feel to every detail.</p>",


    "hero_title": "",
    "twisted_title": "",
    "twisted_text": "",

    "movie_title": "Sustainable Manufacturing",
    "movie_btn": "See the film",
    "movie_src": "/videos/omni.mp4",

    "text_block2_title": "Materials",
    "text_block2": "All of our materials are carefully selected to match our high quality criteria. Also, they should have the smallest environmental impact possible. This means that we want to have few, and long-lasting collaborations with suppliers and manufacturers who share our view on sustainability.",

    "grid": [
      {
        "title": "SUSTAINABLE MATERIALS",
        "text": "All of our materials are carefully chosen to have as little  impact on the environment as possible. To name a few, we use materials such as Lundhags PolyCotton, cellular rubber, high quality leather and mulesing free merino wool.",
        "image": "/img/season/recycledmaterial.jpg"

      },
      {
        "title": "IT’S ALL IN THE DETAILS",
        "text": "To make lasting clothes, boots and backpacks, you need materials you can rely on. This is why we choose only high-quality details down to every buckle, strap, zipper and webbing.",
        "image": "/img/backpacks/metalbuckets.jpg"

      },
      {
        "title": "LONG LASTING CRAFTSMANSHIP",
        "text": "When in the outdoors, there is no room for compromise on materials nor quality. All of our boots, backpacks and clothes are made with world class craftsmanship and high repairability as as the main ingredients.",
        "image": "/img/heritage/worldclasscraftmanshipheritage.jpg"
      },

    ],

    "banner_title": "Sustainability",

    "text_block3": "To us, the core of sustainability is to consume fewer products, of higher quality, and to use them for a longer time. This is why we strive to design and manufacture boots, clothes and backpacks of the highest quality and useful features for you to wear, and tear – and love – for a long time.",

    "clothing_link": "https://www.lundhags.se/en/products/clothing/jackets",
    "clothing_title": "Jackets",
    "clothing_text": "When in the outdoors, comfort and functionality is crucial. All of our jackets are thoroughly worked-through in terms of design, function and features to enhance your experience.<br>	We have updated all of our LPC (Lundhags PolyCotton) fabrics with newly developed LPC fabrics in organic cotton and recycled polyester. This means that a large part of our jackets now are even more sustainable.",

    "clothing_categories": [
      {
        "title": "Backpacks",
        "text": "When developing our new range of backpacks, we had a couple of core components in mind. First of all, we chose truly hard wearing and durable materials of highest quality. We also made them as repairable as possible, in the unfortunate event that they would break. And most of all, we wanted them to have as little impact on the environment as possible.",
        "image": "/img/season/backpacks.jpg",
        "bg_color": "red",
        "link": "https://www.lundhags.se/en/products/packs-bags"
      },
      {
        "title": "Pants",
        "text": "No matter what you are about to do, where you want to go or what you plan to see, there are a couple of key features that will take you there. Curiosity, energy and a will to explore, of course. And comfortable pants with just the right features. No matter what your preferences are, and what you need in order to enhance your experience in the outdoors, we have the pants for you.",
        "image": "/img/season/pants.jpg",
        "bg_color": "red",
        "link": "https://www.lundhags.se/en/products/clothing/pants"
      },
      {
        "title": "Shell boots",
        "text": "Our boots are made according to the shell principle. Instead of putting several layers together, we do what active outdoors people have always done when getting dressed – we keep the layers apart to keep you dry and comfortable through any weather. This means, they will also get dry quickly.",
        "parallax": {
          "image_bg": "/img/view_bg.jpg",
          "image": "/img/view_legs.png"
        },
        "bg_color": "red",
        "link": "https://www.lundhags.se/en/products/kaengor/shell-boots"
      },
    ],

    "omni_title": "Omni boots",
    "omni_link": "https://www.lundhags.se/en/products/kaengor/omni",
    "omni_text": "OMNI is a new range of high-quality boots, originating from the borderland between urban environments and the mountains – and is just as suitable in both worlds. They are developed for people who love the outdoors, and value function and comfort; compromising neither on quality nor Nordic authenticity.",

    "heritage_title": "Heritage boots",
    "heritage_link": "https://www.lundhags.se/en/products/kaengor/heritage",
    "heritage_text": "Heritage collection is inspired by original designs from our past. These traditional work boots have been refined and updated, and are manufactured from the finest materials – and with true attention to details.",

    "tsin_text": "We believe in making the most of every season, and what’s right in front of us. Take every opportunity to enjoy the outdoors and tag your images with #lundhags.",
    "tsin_insta_title": "Follow us",
    "tsin_insta_text": "Continue your Lundhags adventure also on social media – follow us here.",

    "product_link_text_before": "View all",
    "product_link_text_after": "",

    "footer_title": "The season is now",
    "footer_images": [
      {
        "src": "/img/season/theseasonisnowboots.jpg",
        "attr": "let-it-go",
        "offset": "0"
      },
      {
        "src": "/img/season/theseasonisnowboot_1.jpg",
        "attr": "let-it-go horizontal",
        "offset": "200"
      }
    ]

  },
  "sv": {
    "main_title": "DEN BÄSTA ÅRSTIDEN ÄR NU",

    "text_block1": "De stora upplevelserna går inte att schemalägga. Varken det stora äventyret eller de vardagliga utflykterna har inga satta regler eller färdiga ramar – istället är det upp till dig. Ta chansen, och gå ut. Gå längre, upptäck mer och upplev årstidernas variation. Innerst inne så vet du att det faktiskt är precis så enkelt. Låt nu den tanken gå från ord till handling. Packa din ryggsäck, knyt dina kängor och ta vara tillfället.<br>
Den bästa årstiden är just nu.",

    "one_col_title": "FUNKTION<br/>& KOMFORT",
    "one_col_text": "<p>I 85 år har vi sett hur tiderna förändras, både när det kommer till väder, aktiviteter och människors drömmar. Att vi har våra rötter i Jämtland innebär att vi har lärt oss den tuffa vägen hur viktigt det är med pålitliga och bekväma kängor, kläder och utrustning – och att välja saker som är både hållbara och hållbara.</p><p>När du väljer ett par kängor, ett klädesplagg eller en ryggsäck från Lundhags, kan du vara säker på att de kommer hålla under mycket lång tid framöver.</p><p>Våra produkter är gjorda för att du ska trivas med dem – och verkligen använda dem under – många år framöver. Kollektionen är noggrant genomtänkt, och alla produkter är tillverkade av slitstarka material, med goda reparationsmöjligheter och med Lundhagskänsla i varje detalj.</p>",


    "hero_title": "",
    "twisted_title": "",
    "twisted_text": "",

    "movie_title": "Hållbar tillverkning",
    "movie_btn": "Se filmen",
    "movie_src": "/videos/omni.mp4",

    "text_block2_title": "Material",
    "text_block2": "Alla våra material är noggrant utvalda för att uppfylla våra högt ställda krav på kvalitet. Vi vill också att de ska ha så liten miljöpåverkan som möjligt, och vi har valt att ha ett fåtal men långvariga samarbeten med leverantörer och tillverkare som delar vår syn på hållbarhet. ",

    "grid": [
      {
        "title": "HÅLLBARA MATERIAL",
        "text": "Alla våra material är noggrant utvalda för att ha så liten påverkan på miljön som möjligt. Bland annat så använder vi Lundhags PolyCotton, cellgummi, läder av högsta kvalitet och mulesing-fri merinoull.",
        "image": "/img/season/recycledmaterial.jpg"

      },
      {
        "title": "DET HÄNGER PÅ DETALJERNA",
        "text": "För att skapa hållbara kläder, kängor och ryggsäckar behöver man material man kan lita på. Därför väljer vi detaljer av högsta kvalitet till minsta detalj, spänne, band, och blixtlås.",
        "image": "/img/backpacks/metalbuckets.jpg"

      },
      {
        "title": "HANTVERK SOM HÅLLER",
        "text": "I naturen finns det inte plats för kompromisser när det kommer till material och kvalitet. Alla våra kängor, ryggsäckar och kläder är gjorda med hantverk i världsklass, och stora smarta lösningar som att du kan reparera dem när du använt dem under riktigt lång tid.",
        "image": "/img/heritage/worldclasscraftmanshipheritage.jpg"
      },

    ],

    "banner_title": "HÅLLBARHET",

    "text_block3": "För oss handlar hållbarhet om att köpa färre produkter, men av högre kvalitet – och använda dem under en längre tid. Därför strävar vi efter att tillverka kängor, kläder och ryggsäckar av högsta kvalitet, och med en användbar design, som du kan använda och slita under riktigt lång tid framöver.",

    "clothing_link": "https://www.lundhags.se/produkter/klader/jackor",
    "clothing_title": "jackor",
    "clothing_text": "Oavsett hur din utflykt eller ditt äventyr ser ut så är komfort och funktionalitet två avgörande faktorer. Alla våra jackor är noggrant genomarbetade när det kommer till design, funktion och detaljer som ger dig en trivsammare upplevelse. Vi har nu uppdaterat alla våra Lundhags PolyCotton-tyger med ekologiskt bomull och återvunnet polyester. Det betyder att en stor del av våra jackor nu är ännu mer hållbara.",

    "clothing_categories": [
      {
        "title": "ryggsäckar",
        "text": "När vi utvecklade vår nya serie ryggsäckar, hade vi ett par huvudkomponenter i åtanke. Först och främst valde vi väldigt slitstarka och hållbara material av högsta kvalitet. Vi gjorde dem så reparerbara som möjligt, ifall oturen är framme och skulle gå sönder. Dessutom ska de ha så liten påverkan på miljön som möjligt.",
        "image": "/img/season/backpacks.jpg",
        "bg_color": "red",
        "link": "https://www.lundhags.se/produkter/ryggsackar"
      },
      {
        "title": "byxor",
        "text": "Oavsett vad du vill göra, vart du ska åka eller vad du planerar att se, så finns det några grundstenar som kommer att hjälpa dig på vägen: nyfikenhet, energi och en vilja att upptäcka, så klart. Och bekväma byxor med precis rätt funktioner. Oavsett vad du har för önskemål och behov för att kunna känna dig bekväm och torr ute i naturen, så har vi byxorna för dig. ",
        "image": "/img/season/pants.jpg",
        "bg_color": "red",
        "link": "https://www.lundhags.se/produkter/klader/byxor"
      },
      {
        "title": "skalkängor",
        "text": "Våra kängor är gjorda enligt skalprincipen. Istället för att sätta ihop flera olika lager, så gör vi det som aktiva friluftsmänniskor alltid har gjort – vi håller isär de olika lagren så att du ska hålla dig torr och bekväm oavsett väder. Det innebär också att kängorna torkar snabbare. ",
        "parallax": {
          "image_bg": "/img/view_bg.jpg",
          "image": "/img/view_legs.png"
        },
        "bg_color": "red",
        "link": "https://www.lundhags.se/produkter/kangor/skalkangor"
      },
    ],

    "omni_title": "Omni",
    "omni_link": "https://www.lundhags.se/produkter/kangor/omni",
    "omni_text": "OMNI är en ny serie högkvalitativa kängor som har sitt ursprung i gränslandet mellan stad och fjäll – och de passar precis lika bra i båda världar. De är framtagna för dig som älskar att vara utomhus och som värderar funktion och komfort högt; utan att tumma på kvaliteten.",

    "heritage_title": "Heritage",
    "heritage_link": "https://www.lundhags.se/produkter/kangor/heritage",
    "heritage_text": "Vår kollektion Heritage är inspirerad av originaldesignen från vår historia. De här traditionella arbetskängorna har nu uppdaterats och förfinats och är tillverkade av de bästa materialen – och med känsla för detaljerna.",

    "tsin_text": "Vi tror på att att göra det mesta av varje årstid – och det vi har framför oss just nu. Ta tillvara varje möjlighet att njuta av naturen, och tagga dina bilder med #lundhags.",
    "tsin_insta_title": "Följ oss",
    "tsin_insta_text": "Fortsätt ditt Lundhags äventyr på sociala medier – följ oss här.",

    "product_link_text_before": "Visa alla",
    "product_link_text_after": "",

    "footer_title": "DEN BÄSTA ÅRSTIDEN ÄR JUST NU",
    "footer_images": [
      {
        "src": "/img/season/theseasonisnowboots.jpg",
        "attr": "let-it-go",
        "offset": "0"
      },
      {
        "src": "/img/season/theseasonisnowboot_1.jpg",
        "attr": "let-it-go horizontal",
        "offset": "200"
      }
    ]

  },
  "de": {
    "main_title": "Es ist an der Jahreszeit",

    "text_block1": "es gibt keinen Terminkalender für neue Erfahrungen. dein großes Abenteuer hat keine festen Regeln, und keine Voreinstellungen. und die Abenteuer des Alltags genauso wenig. es liegt also an dir. weiterzugehen, weiter zu erkunden, und den Wechsel der Jahreszeiten zu spüren.
nutz‘ die Chance und geh raus. denn du weißt genau, dass es so einfach ist. lass diesen Gedanken zur Handlung werden. nimm deinen Rucksack, schnür deine Stiefel und mach das Beste aus dem, was direkt vor deiner Nase ist.<br>
  es ist an der Jahreszeit",

    "one_col_title": "funktionalität<br/>& tragekomfort",
    "one_col_text": "<p>seit 85 Jahren sehen wir, wie die Jahreszeiten wechseln – und mit ihnen Wetter, Unternehmungen und Träume der Menschen. durch unsere Wurzeln in den eigensinnigen Bergen des schwedischen Jämtlands haben wir gelernt, wie wichtig zuverlässige und bequeme Stiefel, Kleidung und Ausrüstung sind; und natürlich wissen wir, was Nachhaltigkeit wert ist.</p><p>wenn man sich für ein Paar Stiefel, ein Kleidungsstück oder einen Rucksack von Lundhags entscheidet, kann man ganz sicher sein, dass sie den Wechsel der Jahreszeiten überdauern.</p><p>unsere Produkte sind dafür da, dass sie sich angenehm tragen und lange halten; eine ausgereifte, reparaturfreundliche Kollektion aus haltbaren Materialien, mit dem Lundhags-Gefühl bis ins kleinste Detail.</p>",


    "hero_title": "",
    "twisted_title": "",
    "twisted_text": "",

    "movie_title": "nachhaltige materialien",
    "movie_btn": "Film anschauen",
    "movie_src": "/videos/omni.mp4",

    "text_block2_title": "materialien",
    "text_block2": "alle unsere Materialien sind sorgfältig ausgewählt, um unseren hohen Qualitätskriterien gerecht zu werden. außerdem sollen sie die Umwelt so wenig wie möglich belasten. das heißt, dass wir mit wenigen Zulieferern und Herstellern - die unsere Auffassung von Nachhaltigkeit teilen - langfristig zusammenarbeiten.",

    "grid": [
      {
        "title": "nachhaltige materialien",
        "text": "alle unsere Materialien sind sorgfältig ausgewählt, um die Umwelt so wenig wie möglich zu belasten. wir verwenden Materialien wie Lundhags PolyCotton, Zellgummi, hochwertiges Leder und mulesingfreie Merinowolle, um nur einige zu nennen.",
        "image": "/img/season/recycledmaterial.jpg"

      },
      {
        "title": "es geht um‘s detail",
        "text": "für haltbare Kleidung, Stiefel und Rucksäcke benötigt man Materialien, auf die man sich verlassen kann. deshalb verwenden wir ausschließlich hochwertige Details, auch für Schnallen, Riemen, Reißverschlüsse und Gurtbänder.",
        "image": "/img/backpacks/metalbuckets.jpg"

      },
      {
        "title": "gediegenes handwerk",
        "text": "in der Natur ist kein Platz für Kompromisse bei Materialien oder Qualität. für alle unsere Stiefel, Rucksäcke und Kleidungsstücke gilt, dass sie mit handwerklichem Spitzenkönnen gefertigt sind und hohe Reparaturfreundlichkeit ein Muss ist.",
        "image": "/img/heritage/worldclasscraftmanshipheritage.jpg"
      },

    ],

    "banner_title": "nachhaltigkeit",

    "text_block3": "für uns ist das Kernstück von Nachhaltigkeit, weniger - aber dafür höherwertige - Produkte zu kaufen und diese länger zu benutzen. und darum setzen wir auf die Entwicklung und Herstellung von hochwertigen, funktionalen Stiefeln, Kleidungsstücken und Rucksäcken, die man jahrein jahraus tragen und strapazieren -  und lieben - kann.",

    "clothing_link": "https://www.lundhags.se/en/products/clothing/jackets",
    "clothing_title": "jacken",
    "clothing_text": "in der Natur sind Tragekomfort und Funktionalität entscheidend. Design, Funktionen und Eigenschaften aller unserer Jacken sind so ausgeklügelt, dass sie deine Erlebnisse verstärken.
wir haben alle unsere LPC(Lundhags PolyCotton)-Gewebe mit neu entwickelten LPC-Geweben aus Ökobaumwolle und recyceltem Polyester aufgewertet. dadurch sind viele unserer Jacken jetzt noch nachhaltiger.",

    "clothing_categories": [
      {
        "title": "rucksäcke",
        "text": "bei der Entwicklung unseres neuen Rucksacksortiments sind wir von ein paar zentralen Komponenten ausgegangen. zuallererst haben wir uns für richtig strapazierfähige, haltbare Materialien von höchster Qualität entschieden. außerdem haben wir die Rucksäcke so reparaturfreundlich wie möglich gemacht, falls sie unglücklicherweise mal kaputt gehen. und vor allem sollen sie die Umwelt so wenig wie möglich belasten.",
        "image": "/img/season/backpacks.jpg",
        "bg_color": "red",
        "link": "https://www.lundhags.se/en/products/packs-bags"
      },
      {
        "title": "hosen",
        "text": "egal, was du vorhast und wohin du willst, es gibt ein paar entscheidende Eigenschaften, die dich ans Ziel bringen. Neugier, Energie und Entdeckungsfreude, natürlich. und bequeme Hosen mit den richtigen Funktionen. egal, welche Vorstellungen du hast und was du für das ideale Naturerlebnis brauchst, wir haben die richtigen Hosen für dich.",
        "image": "/img/season/pants.jpg",
        "bg_color": "red",
        "link": "https://www.lundhags.se/en/products/clothing/pants"
      },
      {
        "title": "schalenstiefel",
        "text": "unsere Stiefel sind nach dem Schalenprinzip gefertigt. anstatt mehrere Schichten zusammenzufügen, machen wir genau das, was aktive Outdoor-Liebhaber schon immer bei ihrer Kleidung getan haben – wir halten die einzelnen Schichten auseinander, damit man bei jedem Wetter angenehm trocken bleibt. und so trocknen sie auch schnell.",
        "parallax": {
          "image_bg": "/img/view_bg.jpg",
          "image": "/img/view_legs.png"
        },
        "bg_color": "red",
        "link": "https://www.lundhags.se/en/products/kaengor/shell-boots"
      },
    ],

    "omni_title": "omni-stiefel",
    "omni_link": "https://www.lundhags.se/en/products/kaengor/omni",
    "omni_text": "omni ist ein neues Produktprogramm von hochwertigen Stiefeln, die sich zwischen urbanem Milieu und Bergen bewegen – und in beide Welten passen. wir haben sie für Outdoor-Liebhaber entwickelt, die Funktionalität und Tragekomfort schätzen; und die weder an der Qualität noch an dem speziellen skandinavischen Etwas Abstriche machen wollen.",

    "heritage_title": "heritage-stiefel",
    "heritage_link": "https://www.lundhags.se/en/products/kaengor/heritage",
    "heritage_text": "die Heritage-Kollektion ist von Lundhags‘ traditionellem Originaldesign inspiriert. diese veredelte, moderne Version der altehrwürdigen Workboots ist aus auserlesenen Materialien gefertigt – und mit wahrer Liebe zum Detail.",

    "tsin_text": "wir glauben daran, dass man aus jeder Jahreszeit das Beste machen kann, mit genau dem, was man direkt vor der Nase hat. also nichts wie raus in die Natur, und tagg deine Bilder mit #lundhags.",
    "tsin_insta_title": "folge uns",
    "tsin_insta_text": "hier kannst du dein Lundhags-Abenteuer auf sozialen Medien fortsetzen – folge uns",

    "product_link_text_before": "Alles anschauen",
    "product_link_text_after": "",

    "footer_title": "The season is now",
    "footer_images": [
      {
        "src": "/img/season/theseasonisnowboots.jpg",
        "attr": "let-it-go",
        "offset": "0"
      },
      {
        "src": "/img/season/theseasonisnowboot_1.jpg",
        "attr": "let-it-go horizontal",
        "offset": "200"
      }
    ]

  },


}
